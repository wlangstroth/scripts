# scripts

Some demo scripts in Guile scheme

## Why

Guile scripts are a nice alternative to using bash syntax if you're used to
working with lisps.

## Okay

`export GUILE_LOAD_PATH="$GUILE_LOAD_PATH:$PWD"`

That's one way to do a temporary load path hack.
